package com.intens.intenstask.service.impl;

import java.util.List;
import java.util.Set;

import com.intens.intenstask.dto.CandidateDTO;
import com.intens.intenstask.exceptions.EntityNotFoundException;
import com.intens.intenstask.model.Candidate;
import com.intens.intenstask.model.Skill;
import com.intens.intenstask.repository.CandidateRepository;
import com.intens.intenstask.service.CandidateService;
import com.intens.intenstask.service.SkillService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JpaCandidateService implements CandidateService {

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    CandidateRepository candidateRepository;

    @Autowired
    SkillService skillService;

    @Override
    public Candidate findOne(int id) throws EntityNotFoundException {
        return candidateRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public List<Candidate> findAll() {
        return candidateRepository.findAll();
    }

    @Override
    public Candidate save(Candidate candidate) {
        return candidateRepository.save(candidate);
    }

    @Override
    public Candidate delete(int id) throws EntityNotFoundException {
        Candidate candidate = findOne(id);

        for (Skill skill : candidate.getSkills()) {
            skill.getCandidates().remove(candidate);
            skillService.save(skill);
        }

        candidateRepository.delete(candidate);
        return candidate;
    }

    @Override
    public List<Candidate> findByName(String name) {
        return candidateRepository.searchByName(name);
    }

    @Override
    public Candidate edit(int id, CandidateDTO candidateDTO) throws EntityNotFoundException {
        Candidate candidate = findOne(id);
        candidate.getSkills().clear();

        candidateDTO.setId(id);
        candidate = modelMapper.map(candidateDTO, Candidate.class);

        for (int skillID : candidateDTO.getSkillIDs()) {
            candidate.addSkill(skillService.findOne(skillID));
        }

        save(candidate);
        return candidate;
    }

    @Override
    public List<Candidate> findBySkillsName(Set<String> skillsName) {
        return candidateRepository.findBySkillsNameIn(skillsName);
    }

}

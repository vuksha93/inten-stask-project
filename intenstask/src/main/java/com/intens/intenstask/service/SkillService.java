package com.intens.intenstask.service;

import java.util.List;

import com.intens.intenstask.dto.SkillDTO;
import com.intens.intenstask.exceptions.EntityNotFoundException;
import com.intens.intenstask.model.Skill;

public interface SkillService {

    Skill findOne(int id) throws EntityNotFoundException;

    List<Skill> findAll();

    Skill save(Skill skill);

    Skill delete(int id) throws EntityNotFoundException;

    List<Skill> findByName(String name);

    Skill edit(int id, SkillDTO skillDTO) throws EntityNotFoundException;

}

package com.intens.intenstask.service;

import java.util.List;
import java.util.Set;

import com.intens.intenstask.dto.CandidateDTO;
import com.intens.intenstask.exceptions.EntityNotFoundException;
import com.intens.intenstask.model.Candidate;

public interface CandidateService {

    Candidate findOne(int id) throws EntityNotFoundException;

    List<Candidate> findAll();

    Candidate save(Candidate candidate);

    Candidate delete(int id) throws EntityNotFoundException;

    List<Candidate> findByName(String name);

    Candidate edit(int id, CandidateDTO candidateDTO) throws EntityNotFoundException;

    List<Candidate> findBySkillsName(Set<String> skillsName);

}

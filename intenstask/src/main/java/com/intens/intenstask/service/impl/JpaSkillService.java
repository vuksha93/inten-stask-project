package com.intens.intenstask.service.impl;

import java.util.List;

import com.intens.intenstask.dto.SkillDTO;
import com.intens.intenstask.exceptions.EntityNotFoundException;
import com.intens.intenstask.model.Candidate;
import com.intens.intenstask.model.Skill;
import com.intens.intenstask.repository.SkillRepository;
import com.intens.intenstask.service.CandidateService;
import com.intens.intenstask.service.SkillService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JpaSkillService implements SkillService {

    @Autowired
    SkillRepository skillRepository;

    @Autowired
    CandidateService candidateService;

    ModelMapper modelMapper = new ModelMapper();

    @Override
    public Skill findOne(int id) throws EntityNotFoundException {
        return skillRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public List<Skill> findAll() {
        return skillRepository.findAll();
    }

    @Override
    public Skill save(Skill skill) {
        return skillRepository.save(skill);
    }

    @Override
    public Skill delete(int id) throws EntityNotFoundException {
        Skill skill = findOne(id);

        for (Candidate candidate : skill.getCandidates()) {
            candidate.getSkills().remove(skill);
            candidateService.save(candidate);
        }

        skillRepository.delete(skill);
        return skill;
    }

    @Override
    public List<Skill> findByName(String name) {
        return skillRepository.searchByName(name);
    }

    @Override
    public Skill edit(int id, SkillDTO skillDTO) throws EntityNotFoundException {
        Skill skill = findOne(id);

        skillDTO.setId(id);
        skill = modelMapper.map(skillDTO, Skill.class);

        save(skill);
        return skill;
    }

}

package com.intens.intenstask.dto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CandidateDTO {

    private int id;

    @NotBlank(message = "Name is mandatory")
    private String name;

    private LocalDate dateOfBirth;

    @NotBlank(message = "Contact number is mandatory")
    private String contactNumber;

    @NotBlank(message = "Email is mandatory")
    @Email
    private String email;

    private Set<Integer> skillIDs = new HashSet<>();

}

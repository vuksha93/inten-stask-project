package com.intens.intenstask.controller;

import java.lang.reflect.Type;
import java.util.List;

import com.intens.intenstask.dto.SkillDTO;
import com.intens.intenstask.exceptions.EntityNotFoundException;
import com.intens.intenstask.model.Skill;
import com.intens.intenstask.service.SkillService;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/skill")
public class SkillController {

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    SkillService skillService;

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
    public SkillDTO create(@RequestBody SkillDTO skillDTO) {
        Skill skill = modelMapper.map(skillDTO, Skill.class);
        skillService.save(skill);

        return modelMapper.map(skill, SkillDTO.class);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping
    public List<SkillDTO> getAll(@RequestParam(required = false) String name) {
        Type skillsDTO = new TypeToken<List<SkillDTO>>() {
        }.getType();

        if (name == "" || name == null) {
            return modelMapper.map(skillService.findAll(), skillsDTO);
        }

        return modelMapper.map(skillService.findByName(name), skillsDTO);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/{id}")
    public SkillDTO getOne(@PathVariable int id) throws EntityNotFoundException {
        Skill skill = skillService.findOne(id);
        return modelMapper.map(skill, SkillDTO.class);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/{id}")
    public SkillDTO update(@PathVariable int id, @RequestBody SkillDTO skillDTO) throws EntityNotFoundException {

        return modelMapper.map(skillService.edit(id, skillDTO), SkillDTO.class);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/{id}")
    public SkillDTO delete(@PathVariable int id) throws EntityNotFoundException {
        return modelMapper.map(skillService.delete(id), SkillDTO.class);
    }

}

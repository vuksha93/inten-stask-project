package com.intens.intenstask.controller;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;

import com.intens.intenstask.dto.CandidateDTO;
import com.intens.intenstask.exceptions.EntityNotFoundException;
import com.intens.intenstask.model.Candidate;
import com.intens.intenstask.model.Skill;
import com.intens.intenstask.service.CandidateService;
import com.intens.intenstask.service.SkillService;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/candidate")
public class CandidateController {

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    CandidateService candidateService;

    @Autowired
    SkillService skillService;

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
    public CandidateDTO create(@RequestBody CandidateDTO candidateDTO) {
        Candidate candidate = modelMapper.map(candidateDTO, Candidate.class);
        candidateService.save(candidate);

        return modelMapper.map(candidate, CandidateDTO.class);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping
    public List<CandidateDTO> getAll(@RequestParam(required = false) String name) {
        Type candidatesDTO = new TypeToken<List<CandidateDTO>>() {
        }.getType();

        if (name == "" || name == null) {
            return modelMapper.map(candidateService.findAll(), candidatesDTO);
        }

        return modelMapper.map(candidateService.findByName(name), candidatesDTO);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/skillsName")
    public List<CandidateDTO> getBySkillsName(@RequestParam(required = true) Set<String> skillsName) {
        Type candidatesDTO = new TypeToken<List<CandidateDTO>>() {
        }.getType();

        return modelMapper.map(candidateService.findBySkillsName(skillsName), candidatesDTO);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/{id}")
    public CandidateDTO getOne(@PathVariable int id) throws EntityNotFoundException {
        Candidate candidate = candidateService.findOne(id);
        return modelMapper.map(candidate, CandidateDTO.class);
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping("/{candidateID}/addSkill/{skillID}")
    public void addSkill(@PathVariable int candidateID, @PathVariable int skillID) throws EntityNotFoundException {
        Candidate candidate = candidateService.findOne(candidateID);
        Skill skill = skillService.findOne(skillID);

        candidate.addSkill(skill);
        candidateService.save(candidate);
        skillService.save(skill);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/{id}")
    public CandidateDTO update(@PathVariable int id, @RequestBody CandidateDTO candidateDTO)
            throws EntityNotFoundException {

        return modelMapper.map(candidateService.edit(id, candidateDTO), CandidateDTO.class);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/{id}")
    public CandidateDTO delete(@PathVariable int id) throws EntityNotFoundException {
        return modelMapper.map(candidateService.delete(id), CandidateDTO.class);
    }

    @DeleteMapping("/{candidateID}/removeSkill/{skillID}")
    public void deleteSkill(@PathVariable int candidateID, @PathVariable int skillID) throws EntityNotFoundException {
        Candidate candidate = candidateService.findOne(candidateID);
        Skill skill = skillService.findOne(skillID);

        candidate.removeSkill(skill);
        candidateService.save(candidate);
        skillService.save(skill);
    }

}

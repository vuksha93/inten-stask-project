package com.intens.intenstask.repository;

import java.util.List;
import java.util.Set;

import com.intens.intenstask.model.Candidate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Integer> {

    @Query("SELECT c FROM Candidate c WHERE c.name LIKE %:name%")
    List<Candidate> searchByName(@Param("name") String name);

    List<Candidate> findBySkillsNameIn(Set<String> skillsName);

}

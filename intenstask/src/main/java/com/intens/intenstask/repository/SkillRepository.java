package com.intens.intenstask.repository;

import java.util.List;

import com.intens.intenstask.model.Skill;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Integer> {

    @Query("SELECT s FROM Skill s WHERE s.name LIKE %:name%")
    List<Skill> searchByName(@Param("name") String name);

}

package com.intens.intenstask.support;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;

import com.intens.intenstask.model.Candidate;
import com.intens.intenstask.model.Skill;
import com.intens.intenstask.service.CandidateService;
import com.intens.intenstask.service.SkillService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestData {

    private final int DATA_NUMBER = 5;

    @Autowired
    CandidateService candidateService;

    @Autowired
    SkillService skillService;

    @PostConstruct
    public void addData() {
        List<Candidate> candidates = new ArrayList<>();
        List<Skill> skills = new ArrayList<>();
        LocalDate date = LocalDate.of(1993, 2, 4);

        for (int i = 1; i <= DATA_NUMBER; i++) {
            Candidate candidate = new Candidate(i, "New Name " + i, date, "111222" + i, "email" + i + "@test.com",
                    new HashSet<>());
            candidates.add(candidate);
            candidateService.save(candidate);

            Skill skill = new Skill(i, "Skill " + i, new HashSet<>());
            skills.add(skill);
            skillService.save(skill);

            candidate.addSkill(skill);
            candidateService.save(candidate);
            skillService.save(skill);
        }

    }

}

package com.intens.intenstask.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.intens.intenstask.exceptions.EntityNotFoundException;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode.Exclude;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Name is mandatory")
    private String name;

    private LocalDate dateOfBirth;

    @NotBlank(message = "Contact number is mandatory")
    private String contactNumber;

    @NotBlank(message = "Email is mandatory")
    @Email
    private String email;

    @Exclude
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "candidate_skills", joinColumns = { @JoinColumn(name = "candidate_id") }, inverseJoinColumns = {
            @JoinColumn(name = "skill_id") })
    private Set<Skill> skills = new HashSet<>();

    public void addSkill(Skill skill) {
        skills.add(skill);
        skill.getCandidates().add(this);
    }

    public void removeSkill(Skill skill) throws EntityNotFoundException {
        if (!skills.contains(skill)) {
            throw new EntityNotFoundException();
        }

        skills.remove(skill);
        skill.getCandidates().remove(this);
    }

}

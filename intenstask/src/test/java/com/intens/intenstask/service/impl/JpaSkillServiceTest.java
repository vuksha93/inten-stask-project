package com.intens.intenstask.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import com.intens.intenstask.dto.SkillDTO;
import com.intens.intenstask.exceptions.EntityNotFoundException;
import com.intens.intenstask.model.Skill;
import com.intens.intenstask.repository.SkillRepository;
import com.intens.intenstask.service.CandidateService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

@ExtendWith(MockitoExtension.class)
public class JpaSkillServiceTest {

	@InjectMocks
	JpaSkillService jpaSkillService;

	@Mock
	SkillRepository skillRepository;

	@Mock
	CandidateService candidateService;

	private static Skill skill = new Skill(1, "Skill", new HashSet<>());

	ModelMapper modelMapper = new ModelMapper();

	@Test
	public void shouldFindOne() throws EntityNotFoundException {
		when(skillRepository.findById(skill.getId())).thenReturn(Optional.of(skill));
		Skill actualValue = jpaSkillService.findOne(skill.getId());

		assertEquals(skill, actualValue);
	}

	@Test
	public void shouldFindAll() {
		List<Skill> skills = new ArrayList<>();
		skills.add(skill);

		when(skillRepository.findAll()).thenReturn(skills);
		List<Skill> actualValue = jpaSkillService.findAll();

		assertFalse(actualValue.isEmpty());
		assertEquals(1, actualValue.size());
		assertEquals(skills, actualValue);
	}

	@Test
	public void shouldSave() throws EntityNotFoundException {
		when(skillRepository.save(skill)).thenReturn(skill);
		Skill actualValue = jpaSkillService.save(skill);
		when(skillRepository.findById(skill.getId())).thenReturn(Optional.of(skill));
		Skill expectedValue = jpaSkillService.findOne(skill.getId());

		assertNotNull(actualValue);
		assertEquals(expectedValue, actualValue);
	}

	@Test
	public void shouldDelete() throws EntityNotFoundException {
		when(skillRepository.findById(skill.getId())).thenReturn(Optional.of(skill));
		Skill actualValue = jpaSkillService.delete(skill.getId());
		when(skillRepository.findById(skill.getId())).thenReturn(Optional.empty());

		EntityNotFoundException exception = assertThrows(EntityNotFoundException.class,
				() -> jpaSkillService.findOne(skill.getId()));

		String expectedMessage = "Entity not found!";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
		verify(skillRepository, times(1)).delete(actualValue);
	}

	@Test
	public void shouldFindByName() {
		List<Skill> skills = new ArrayList<>();
		skills.add(skill);

		when(skillRepository.searchByName(skill.getName())).thenReturn(skills);
		List<Skill> actualValue = jpaSkillService.findByName(skill.getName());

		assertNotNull(actualValue);
		assertFalse(actualValue.isEmpty());
		assertEquals(1, actualValue.size());
		assertEquals(skills, actualValue);
	}

	@Test
	public void shouldEdit() throws EntityNotFoundException {
		when(skillRepository.save(skill)).thenReturn(skill);
		skill = jpaSkillService.save(skill);
		SkillDTO skillDTO = new SkillDTO(skill.getId(), "Edited skill");

		when(skillRepository.findById(skill.getId())).thenReturn(Optional.of(skill));
		Skill editedValue = jpaSkillService.edit(skill.getId(), skillDTO);
		when(skillRepository.findById(skill.getId())).thenReturn(Optional.of(editedValue));
		Skill actualValue = jpaSkillService.findOne(editedValue.getId());

		assertEquals(editedValue, actualValue);
	}
}

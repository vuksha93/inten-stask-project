package com.intens.intenstask.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import com.intens.intenstask.dto.CandidateDTO;
import com.intens.intenstask.exceptions.EntityNotFoundException;
import com.intens.intenstask.model.Candidate;
import com.intens.intenstask.repository.CandidateRepository;
import com.intens.intenstask.service.SkillService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class JpaCandidateServiceTest {

	@InjectMocks
	JpaCandidateService jpaCandidateService;

	@Mock
	CandidateRepository candidateRepository;

	@Mock
	SkillService skillService;

	private static Candidate candidate = new Candidate(1, "Candidate", LocalDate.now(), "111222", "email@email.com",
			new HashSet<>());

	@Test
	public void shouldFindOne() throws EntityNotFoundException {
		when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(candidate));
		Candidate actualValue = jpaCandidateService.findOne(candidate.getId());

		assertEquals(candidate, actualValue);
	}

	@Test
	public void shouldFindAll() {
		List<Candidate> candidates = new ArrayList<>();
		candidates.add(candidate);

		when(candidateRepository.findAll()).thenReturn(candidates);
		List<Candidate> actualValue = jpaCandidateService.findAll();

		assertFalse(actualValue.isEmpty());
		assertEquals(1, actualValue.size());
		assertEquals(candidates, actualValue);
	}

	@Test
	public void shouldSave() throws EntityNotFoundException {
		when(candidateRepository.save(candidate)).thenReturn(candidate);
		Candidate actualValue = jpaCandidateService.save(candidate);
		when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(candidate));
		Candidate expectedValue = jpaCandidateService.findOne(candidate.getId());

		assertEquals(expectedValue, actualValue);
	}

	@Test
	public void shouldDelete() throws EntityNotFoundException {
		when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(candidate));
		Candidate actualValue = jpaCandidateService.delete(candidate.getId());
		when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.empty());

		EntityNotFoundException exception = assertThrows(EntityNotFoundException.class,
				() -> jpaCandidateService.findOne(candidate.getId()));

		String expectedMessage = "Entity not found!";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));

		verify(candidateRepository, times(1)).delete(actualValue);
	}

	@Test
	public void shouldFindByName() {
		List<Candidate> candidates = new ArrayList<>();
		candidates.add(candidate);

		when(candidateRepository.searchByName(candidate.getName())).thenReturn(candidates);
		List<Candidate> actualValue = jpaCandidateService.findByName(candidate.getName());

		assertFalse(actualValue.isEmpty());
		assertEquals(1, actualValue.size());
		assertEquals(candidates, actualValue);
	}

	@Test
	public void shouldEdit() throws EntityNotFoundException {

	}

	@Test
	public void shouldFindBySkillsName() throws EntityNotFoundException {
		when(candidateRepository.save(candidate)).thenReturn(candidate);
		candidate = jpaCandidateService.save(candidate);
		CandidateDTO candidateDTO = new CandidateDTO(candidate.getId(), "Edited candidate", LocalDate.now(), "111222",
				"email@email.com", new HashSet<>());

		when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(candidate));
		Candidate editedValue = jpaCandidateService.edit(candidate.getId(), candidateDTO);
		when(candidateRepository.findById(candidate.getId())).thenReturn(Optional.of(editedValue));
		Candidate actualValue = jpaCandidateService.findOne(editedValue.getId());

		assertEquals(editedValue, actualValue);
	}
}

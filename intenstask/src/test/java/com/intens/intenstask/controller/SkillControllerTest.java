package com.intens.intenstask.controller;

import com.intens.intenstask.exceptions.EntityNotFoundException;
import com.intens.intenstask.service.SkillService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(value = SkillController.class)
public class SkillControllerTest {

	// TODO

	@Autowired
	MockMvc mockMvc;

	@InjectMocks
	private SkillController skillController;

	@Mock
	SkillService skillService;

	// private static Skill skill = new Skill(1, "Skill", new HashSet<>());
	ModelMapper modelMapper = new ModelMapper();

	@Test
	public void shouldCreate() {
		// TODO: initialize args
		// SkillDTO skillDTO;

		// SkillDTO actualValue = skillController.create(skillDTO);

		// TODO: assert scenario
	}

	@Test
	public void shouldGetAll() {
		// TODO: initialize args
		// String name;

		// List<SkillDTO> actualValue = skillController.getAll(name);

		// TODO: assert scenario
	}

	@Test
	public void shouldGetOne() throws EntityNotFoundException {
		// when(skillService.findOne(skill.getId())).thenReturn(skill);
		// SkillDTO actualValue = skillController.getOne(skill.getId());

		// assertNotNull(actualValue);
		// assertEquals(modelMapper.map(skill, SkillDTO.class), actualValue);
	}

	@Test
	public void shouldUpdate() throws EntityNotFoundException {
		// TODO: initialize args
		// int id;
		// SkillDTO skillDTO;

		// SkillDTO actualValue = skillController.update(id, skillDTO);

		// TODO: assert scenario
	}

	@Test
	public void shouldDelete() throws EntityNotFoundException {
		// TODO: initialize args
		// int id;

		// SkillDTO actualValue = skillController.delete(id);

		// TODO: assert scenario
	}
}

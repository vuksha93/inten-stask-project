package com.intens.intenstask.controller;

import com.intens.intenstask.exceptions.EntityNotFoundException;
import com.intens.intenstask.service.CandidateService;
import com.intens.intenstask.service.SkillService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CandidateControllerTest {

	// TODO

	@InjectMocks
	private CandidateController candidateController;

	@Mock
	CandidateService candidateService;

	@Mock
	SkillService skillService;

	@Test
	public void shouldCreate() {
		// TODO: initialize args
		// CandidateDTO candidateDTO;

		// CandidateDTO actualValue = candidateController.create(candidateDTO);

		// TODO: assert scenario
	}

	@Test
	public void shouldGetAll() {
		// TODO: initialize args
		// String name;

		// List<CandidateDTO> actualValue = candidateController.getAll(name);

		// TODO: assert scenario
	}

	@Test
	public void shouldGetBySkillsName() {
		// TODO: initialize args
		// Set<String> skillsName;

		// List<CandidateDTO> actualValue =
		// candidateController.getBySkillsName(skillsName);

		// TODO: assert scenario
	}

	@Test
	public void shouldGetOne() throws EntityNotFoundException {
		// TODO: initialize args
		// int id;

		// CandidateDTO actualValue = candidateController.getOne(id);

		// TODO: assert scenario
	}

	@Test
	public void shouldAddSkill() throws EntityNotFoundException {
		// TODO: initialize args
		// int candidateID;
		// int skillID;

		// candidateController.addSkill(candidateID, skillID);

		// TODO: assert scenario
	}

	@Test
	public void shouldUpdate() throws EntityNotFoundException {
		// TODO: initialize args
		// int id;
		// CandidateDTO candidateDTO;

		// CandidateDTO actualValue = candidateController.update(id, candidateDTO);

		// TODO: assert scenario
	}

	@Test
	public void shouldDelete() throws EntityNotFoundException {
		// TODO: initialize args
		// int id;

		// CandidateDTO actualValue = candidateController.delete(id);

		// TODO: assert scenario
	}

	@Test
	public void shouldDeleteSkill() {
		// TODO: initialize args
		// int candidateID;
		// int skillID;

		// candidateController.deleteSkill(candidateID, skillID);

		// TODO: assert scenario
	}
}
